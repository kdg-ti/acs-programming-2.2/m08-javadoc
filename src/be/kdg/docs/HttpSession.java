package be.kdg.docs;

public class HttpSession {

  private boolean status;

  /**
   * Create a <b>new</b> Session.
   *
   * @param status is this session <i>valid</i>?
   */
  public void HttpSession(boolean status) {
    this.status =status;
  }


  /**
   * Checks if a session is valid.
   *
   * @return true if the session is valid, false otherwise
   * @deprecated since version 1.1, use the
   * {@link #isValid() isValid} method.
   */


  public boolean valid() {
    return status;
  }

  /**
   * Checks if a session is valid.
   *
   * @return true if the session is valid, false otherwise
   */
  public boolean isValid() {
    return status;
  }

  }
