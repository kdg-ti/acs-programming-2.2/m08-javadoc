package be.kdg.docs;

public class Date {
    private int day;
    private int month;
    private int year;

    /**
     * Makes a Date based on day, month, year
     * @param day Day of the month
     * @param month Month (1-12)
     * @param year Year (Gregorian Calendar)
     */
    public Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    /**
     * Tests of a date is valid.
     * Dates before 1600 are invalid.
     *
     * @param day   D day in month. Validity depends on month and year
     * @param month The month (1-12)
     * @param year  The year (1600-2100)
     * @return true if date is valid, otherwis false
     */
    public static boolean isValidDate(int day, int month, int year) {
        return true;
    }
}
