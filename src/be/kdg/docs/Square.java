package be.kdg.docs;

public class Square {
  int side;


  /**
   * Creates a Square.
   * @param side Side of the square. Must be > 0.
   */
  public Square(int side) {
    this.side = side;
  }
}
