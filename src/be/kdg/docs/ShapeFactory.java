package be.kdg.docs;

/**
 *  The shapeFactory generates shapes
 *  that can be displayed on the local platform
 *  <pre>
 *   Example:
 *      Square square =  ShapeFactory.makeSquare (4.0);
 *      Rectangle box =  ShapeFactory.makeRectangle(3.0, 2.0);
 *      Square square =  ShapeFactory.makeShape(ShapeType.SQUARE, 4.0);
 *  </pre>
 */
public class ShapeFactory {}
